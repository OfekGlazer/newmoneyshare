import { NativeStackNavigationProp, } from '@react-navigation/native-stack';
import { RouteProp } from '@react-navigation/native';

import { MainRoutes, MainStackParamList } from './routes'

export type MainNavigationProp<
    RouteName extends keyof MainStackParamList = MainRoutes
    > = NativeStackNavigationProp<MainStackParamList, RouteName>

export type MainRouteProp<
    RouteName extends keyof MainStackParamList = MainRoutes
    > = RouteProp<MainStackParamList, RouteName>

// export type ProfileScreenRouteProp = RouteProp<MainStackParamList, extend keyof MainStackParamList>;

export type Props<
    RouteName extends keyof MainStackParamList = MainRoutes
    > = {
        navigation: MainNavigationProp<RouteName>
        route: MainRouteProp<RouteName>;
    };