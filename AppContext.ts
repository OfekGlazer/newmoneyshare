import React from "react";
import { Group, User } from "./Models/General";

export interface AppContext {
    user: User | undefined | null,
    setUser: React.Dispatch<React.SetStateAction<User | null | undefined>>
    groupInWork: Group | undefined
    setGroupInWork: React.Dispatch<React.SetStateAction<Group | undefined >>
}

export interface InitalizeAppContext {
    user: User,
    setUser: React.Dispatch<React.SetStateAction<User>>
    groupInWork: Group 
    setGroupInWork: React.Dispatch<React.SetStateAction<Group>>
}

export const userContext = React.createContext<AppContext>({ user: null, setUser: () => null, groupInWork: undefined, setGroupInWork: () => undefined })
