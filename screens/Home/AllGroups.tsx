
import { MaterialIcons, AntDesign } from "@expo/vector-icons";
import React, { useState, useEffect, createContext, useContext } from "react";
import { StyleSheet, Text, View, TextInput, Alert, TouchableOpacity, FlatList } from "react-native";
import { Button, Divider } from "react-native-paper";
import { getAllGroups } from "../../Api/Queries";
import { AppContext, userContext } from "./../../AppContext";
import { Group, User } from "../../Models/General";
import { initialcategories } from "../Service";
import { firebase } from "./../../firebase/config";
import { MainNavigationProp } from "./../../Routing/types";
import { MainRoutes } from "./../../Routing/routes";
import { Item } from "react-native-paper/lib/typescript/components/List/List";

interface Props {
    navigation: MainNavigationProp<MainRoutes.Home>
}

const db = firebase.firestore()

const AllGroups: React.FC<Props> = ({ navigation }) => {

    const [groups, setGroups] = useState<Group[]>([]);

    const globalUser = useContext(userContext).user as User
    const setGroupInWork = useContext(userContext).setGroupInWork
    const appContext: AppContext = useContext(userContext)

    useEffect(() => {
        const currentGroups: any[] = []
        db.collection("Groups").where("userID", "==", globalUser.id)
            .onSnapshot(
                querySnapshot => {
                    // debugger;
                    const newEntities: Group[] = []
                    querySnapshot.forEach(doc => {
                        const entity: Partial<Group> = doc.data()
                        entity.id = doc.id
                        newEntities.push(entity as Group)
                    });
                    // console.log(newEntities)
                    // if(appContext.groupInWork != undefined){
                    // const currGroup = newEntities.find(group => group.id == appContext.groupInWork?.id) as Group
                    // appContext.setGroupInWork({ ...currGroup })
                    // console.log("set group in work")
                    // console.log({ ...currGroup })
                    // }
                    setGroups(newEntities)
                },
                error => {
                    console.log(error)
                }
            )
    }, [])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Groups",
            headerRight: () => (
                <Button onPress={() => { navigation.navigate(MainRoutes.AddGroup) }} color="#2970E9" >
                    add Group
                </Button>
            ),
        });
    }, [navigation,]);

    const groupClicked = (group: Group) => {
        // debugger;
        appContext.setGroupInWork(group)
        // appContext.groupInWork = group;
        navigation.navigate(MainRoutes.MenuGroup, { group: group })
    }

    const Item = ({ title }) => (
        <View style={{    backgroundColor: '#f9c2ff', padding: 20,marginVertical: 8,marginHorizontal: 16, width: 350}}>
          <Text style={{ fontSize: 32,}}>{title}</Text>
        </View>
      );

    const renderItem = ({ item }) => (
        <Item title={item.title} />
      );
    

    const DATA = [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'First Item',
        },
        {
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Second Item',
        },
        {
          id: '58694a0f-3da1-471f-bd96-145571e29d72',
          title: 'Third Item',
        },
      ];
      


    return (

        <View >
            {/* <Text>Total Groups{groups.length}</Text> */}
            {groups.map(group => {
                return (
                    <TouchableOpacity key={group.description + group.members[0]} onPress={() => groupClicked(group)}>
                        <View style={{ flexDirection: "row", marginTop: 10, marginLeft: 10, justifyContent: "space-between" }}>
                            <View style={{ flexDirection: "row" }}>

                                <MaterialIcons name={group.category.icon as any} size={40} color="#2970E9" ></MaterialIcons>
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={{ fontWeight: "400", fontSize: 16 }}>{group.title}</Text>
                                    <Text style={{ fontWeight: "100", fontSize: 12 }}>{group.description}</Text>
                                </View>
                            </View>
                            <AntDesign name="right" style={{ marginTop: 15, marginRight: 15 }}></AntDesign>
                        </View>
                        <Divider style={{ marginTop: 10 }} />
                    </TouchableOpacity>
                )
            })}

<FlatList
        horizontal={true}
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
      />

            {/* <FlatList
                horizontal
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}
                legacyImplementation={false}
                data={["ofek", "omer"]}
                renderItem={item => { return (<View>{item}</View>) }}
                keyExtractor={Item => Item}
                style={{ width: 300, height: 300 }}
            /> */}

        </View>

    );
}

const styles = StyleSheet.create({
});

export default AllGroups;
