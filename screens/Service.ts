import { User } from "../Models/General";
import { firebase } from "./../firebase/config";
import { createContext } from "react";

export const checkUserSession = (setUser: React.Dispatch<React.SetStateAction<User | undefined | null>>) => {

    const usersRef = firebase.firestore().collection("users");
    firebase.auth().onAuthStateChanged((user) => {
        if (user) {
            usersRef
                .doc(user.uid)
                .get()
                .then((document) => {
                    const userData = (document.data() as User);
                    setUser(userData);
                })
                .catch((error) => {
                    setUser(null);
                    console.log(error)
                });
        } else {
            setUser(null);
        }
    });
}

export const initialcategories = [
    {
        name: "Trip",
        icon: "airplanemode-on",
        isSelected: false,
    },
    {
        name: "Shared House",
        icon: "house",
        isSelected: false,
    },
    {
        name: "Couple",
        icon: "people",
        isSelected: false,
    },
    {
        name: "Party",
        icon: "celebration",
        isSelected: false,
    },
    {
        name: "Other",
        icon: "bookmark",
        isSelected: false,
    },
]