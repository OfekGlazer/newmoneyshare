import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, Text, View, TextInput } from "react-native";
// import { Icon } from "react-native-elements";
import { Button } from "react-native-paper";
import { Feather, AntDesign } from "@expo/vector-icons";
import { firebase } from "./../../firebase/config";
import Icon from 'react-native-vector-icons/Ionicons';
import Ofek from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesignIcons from 'react-native-vector-icons/AntDesign';
import DateTimePicker from '@react-native-community/datetimepicker';

interface Props {
    navigation: any
}

const Login: React.FC<Props> = (props) => {
    const listOfTasks = ["das", "dasdas", "DAsgas"];
    const [date, onChangeDate] = useState(new Date());
    const [email, onChangeEmail] = useState("");
    const [password, onChangePassword] = useState("");
    const signUpNavigator = () => {
        // navigation.navigate("Task", {
        //   username: userName,
        // });
        props.navigation.navigate("Signup");
    };

    const onLoginPress = () => {

        console.log(email)
        console.log(password)

        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then((response) => {
                const uid = response.user?.uid;
                const usersRef = firebase.firestore().collection("users");
                usersRef
                    .doc(uid)
                    .get()
                    .then((firestoreDocument) => {
                        if (!firestoreDocument.exists) {
                            console.log("User does not exist anymore.");
                            return;
                        }
                        const user = firestoreDocument.data();
                        props.navigation.navigate("Home", { user });
                    })
                    .catch((error) => {
                        console.log("1")
                        console.log(error)
                    });
            })
            .catch((error) => {
                console.log("2")
                console.log(error)
            });
    };

    return (
        // <SafeAreaView>

        <View style={styles.container}>
            <Text style={styles.appTitle}>MoneyShare</Text>

            <View style={styles.fieldWithIcon}>
                <AntDesign name="user" color="black" size={24} style={styles.iconCheck} />
                <TextInput
                    style={styles.inputText}
                    onChangeText={onChangeEmail}
                    value={email}
                    placeholder="Email.."
                />
            </View>

            <View style={styles.fieldWithIcon}>
                <AntDesign name="lock" size={24} color="black" style={styles.iconCheck} />
                <TextInput
                    style={styles.inputText}
                    onChangeText={onChangePassword}
                    value={password}
                    placeholder="Password.."
                />
            </View>

            <Button style={styles.button} onPress={onLoginPress}>
                Login
            </Button>
            <Text style={styles.title} onPress={signUpNavigator}>
                Signup
            </Text>
        </View>

        /* </SafeAreaView> */
    );
};

const styles = StyleSheet.create({
    searchSection: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
    },
    iconCheck: {
        marginTop: 17,
    },
    fieldWithIcon: {
        // flex: 6,
        width: "85%",
        flexDirection: "row",
    },

    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: "40%"
        // backgroundColor: "#E8EAED",
        // marginBottom: "30px",
    },
    appTitle: {
        fontWeight: "bold",
        fontSize: 40,
        paddingBottom: 15,
    },
    inputText: {
        height: 40,
        width: "100%",
        margin: 12,
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
    },
    password: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
    button: {
        marginTop: 30,
        backgroundColor: "#F55252",
        width: "85%",
        borderRadius: 10,
    },
    title: {
        color: "#acacac",
        fontWeight: "bold",
        marginTop: 10,
        textDecorationLine: "underline",
    },
});

export default Login;
