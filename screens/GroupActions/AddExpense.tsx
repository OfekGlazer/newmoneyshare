import React, { useState, useEffect, createElement, useContext } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import { Button, Chip, TextInput } from "react-native-paper";
import { updateGroup } from "../../Api/Queries";
import { AppContext, userContext } from "../../AppContext";
import { Expanse, Group } from "../../Models/General";
import { MainRoutes } from "../../Routing/routes";
import { Props } from "./../../Routing/types";


const AddExpense: React.FC<Props<MainRoutes.AddExpense>> = ({ navigation, route }) => {

    const appContext: AppContext = useContext(userContext)

    const [group] = useState<Group>(route.params.group as Group);

    const [expanse, setExpanse] = useState<Expanse>({ title: "", paidBy: "", partialPayments: [], amount: "", date: new Date() });
    const [disableSave, setDisableSave] = useState(true);


    const handleChangeExpanse = (prop: keyof Expanse) => (value: string) => {
        setExpanse({ ...expanse, [prop]: value });
    };

    const handleAmountChange = (value: string) => {
        setExpanse({ ...expanse, amount: value.replace(/[^0-9]/g, ''), })
    }

    const saveExpanse = () => {
        // debugger;
        updateGroup({ expanses: [...group.expanses, expanse] }, group.id).then(() => {
            console.log("Document successfully updated!");
        })
            .catch((error) => {
                console.error("Error updating document: ", error);
            });
        route.params.setGroup({ ...group, expanses: [...group.expanses, expanse] })
        navigation.goBack()
    }

    const validateExpanse = () => {
        setDisableSave(!checkExpanse)
    }

    const checkExpanse = () => {
        return expanse.title != "" && expanse.amount != "" && expanse.paidBy != ""
    }

    const splitAmountByMembers = () => {

    }



    const splitEquallyText = (): string => {

        if (expanse.amount == "") return ""

        let splitText = "Split " + expanse.amount + " into " + group.members.length + " members, ";
        const partialAmount = parseInt(expanse.amount) / group.members.length
        group.members.forEach(member => splitText += member + ": " + partialAmount + ", ")
        return splitText
    }

    useEffect(validateExpanse, [expanse])

    React.useLayoutEffect(() => {
        navigation.setOptions({
            title: "Expense",
            headerRight: () => (
                <Button onPress={saveExpanse} color="#2970E9" disabled={disableSave} >Save</Button>
            )
        });
    }, [navigation, disableSave]);

    return (
        <View style={{ flex: 1 }}>
            <TextInput
                dense
                style={styles.inputText}
                onChangeText={handleChangeExpanse('title')}
                value={expanse.title}
                placeholder="Title"
            />
            <TextInput
                style={styles.inputText}
                keyboardType="numeric"
                onChangeText={handleAmountChange}
                value={expanse.amount}
                placeholder="Amount"
            />
            <TextInput
                style={styles.inputText}
                onChangeText={handleChangeExpanse('paidBy')}
                value={expanse.paidBy}
                placeholder="Paid By"
                onFocus={() => navigation.navigate(MainRoutes.ChoosePayingMember, { members: group.members, expnase: expanse, setExpanse: setExpanse })}
            />
            <Text>Split options</Text>
            <View style={{ flexDirection: "row" }}>
                <Chip style={{ marginLeft: 5, marginTop: 5 }}>Split equally</Chip>
                <Chip style={{ marginLeft: 5, marginTop: 5 }}
                    onPress={() => navigation.navigate(MainRoutes.SplitExpense, { expnase: expanse, members: group.members, setExpanse: setExpanse })} >More</Chip>
            </View>
            <View>
                <Text>{splitEquallyText()}</Text>
            </View>

        </View>

    );
};

interface DateProps {
    value: Date,
    onChange: (event: any) => void
}

// const DateTimePicker = ({ value, onChange }: DateProps) => {
//     return createElement('input', {
//         type: 'date',
//         value: value,
//         placeholder: "Search...",
//         onChange: onChange,
//         style: { width: "85%", height: 40, border: 0, backgroundColor: "white", marginLeft: 12, marginTop: 10 }
//     })
// }

const styles = StyleSheet.create({
    inputText: {
        height: 40,
        width: "85%",
        margin: 12,
        backgroundColor: "white"
        // borderBottomWidth: 1,
        // borderColor: "grey",
        // borderRadius: 1,
        // padding: 10,
    },

});
export default AddExpense;
