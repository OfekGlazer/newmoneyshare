
import React, { useState, useEffect, createElement, useContext } from "react";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, FlatList } from "react-native";
import { Checkbox, Chip, Divider, TextInput, } from "react-native-paper";
import { Item } from "react-native-paper/lib/typescript/components/List/List";
import { Group, PartialPayment } from "../../Models/General";
import { MainRoutes } from "../../Routing/routes";
import { Props } from "../../Routing/types";

interface splitMemberInExpanse {
    status: SplitStatus,
    member: string,
    amount: number,
    devideWay: string,
}

enum SplitOption {
    Equally = "Equally",
    Proportionally = "Proportionally",
    Amount = "Amount"
}

enum SplitStatus {
    checked = "checked",
    indeterminate = "indeterminate",
    unchecked = "unchecked"
}


const SplitExpense: React.FC<Props<MainRoutes.SplitExpense>> = ({ navigation, route }) => {

    const members = route.params.members
    const expanse = route.params.expnase
    const portions = members.length
    const amount = expanse.amount

    const [splitMembers, setSplitMembers] = useState<splitMemberInExpanse[]>([])
    const [splitBY, setSplitBy] = useState<SplitOption>(SplitOption.Equally)
    const [checkedMembers, setCheckedMembers] = useState<number>(0)



    useEffect(() => {
        const initialSplitMembers: splitMemberInExpanse[] = members.map((member) => {
            return { status: SplitStatus.checked, member: member, amount: parseInt(amount) / portions, devideWay: "1" }
        })
        setSplitMembers(initialSplitMembers)
        setCheckedMembers(initialSplitMembers.filter(member => member.status == SplitStatus.checked).length)
    }, [])

    useEffect(() => {
        setCheckedMembers(splitMembers.filter(member => member.status == SplitStatus.checked).length)
    }, [splitMembers])

    useEffect(() => {
        const calcSplitMember = [...splitMembers]
        // calcSplitMember.f

    }, [splitMembers, splitBY])

    const calcAllsplitPayments = () => {
        let splitPayment: splitMemberInExpanse[] = []
        if (splitBY == SplitOption.Equally) {

        }
        splitPayment = splitMembers.map<splitMemberInExpanse>(currMember => {
            const initPayment = { member: currMember.member, status: currMember.status, amount: 0, devideWay: "0" }
            const payment: splitMemberInExpanse = initPayment

            // uncheck member so set to zero..
            if (currMember.status == SplitStatus.unchecked) return payment


            if (splitBY == SplitOption.Equally) {
                payment.devideWay = "1"
                payment.amount = parseInt(route.params.expnase.amount) / checkedMembers
            } else if (splitBY == SplitOption.Amount) {

                payment.devideWay = "1"
                payment.amount = parseInt(route.params.expnase.amount) / checkedMembers
            }
            return payment
        })
    }

    const handleSplitChangeInput = (member: string) => (value: string) => {
        splitMembers.forEach(splitMember => {
            if (splitMember.member == member) {
                splitMember.devideWay = value
            }
        })

        setSplitMembers([...splitMembers])
    }

    const handleSplitChangeCheckBox = (member: string) => () => {
        splitMembers.forEach(splitMember => {
            if (splitMember.member == member) {
                splitMember.status = (splitMember.status == SplitStatus.checked) ? SplitStatus.unchecked : SplitStatus.checked
            }
        })
        setSplitMembers([...splitMembers])
    }

    const calcAmountPerMember = () => {

    }


    return (
        <View style={{}}>

            <Text style={{ fontWeight: "bold", fontSize: 18, marginTop: 15, marginLeft: 10 }}>
                {"Split " + expanse.amount + " into " + members.length + " portion(s)"}
            </Text>
            <View style={{ flexDirection: "row", marginTop: 15, }}>
                <Chip style={[{ marginLeft: 5, marginTop: 5 }, splitBY == SplitOption.Equally ? { backgroundColor: "#69B0EF" } : {}]}
                    onPress={() => setSplitBy(SplitOption.Equally)} >Split Equally</Chip>
                <Chip style={[{ marginLeft: 5, marginTop: 5 }, splitBY == SplitOption.Proportionally ? { backgroundColor: "#69B0EF" } : {}]}
                    onPress={() => setSplitBy(SplitOption.Proportionally)}>Split Proportionally</Chip>
                <Chip style={[{ marginLeft: 5, marginTop: 5 }, splitBY == SplitOption.Amount ? { backgroundColor: "#69B0EF" } : {}]}
                    onPress={() => setSplitBy(SplitOption.Amount)}>Split By amount</Chip>
                {/* <Chip style={{ marginLeft: 5, marginTop: 5 }} >Split proportionally</Chip>
                <Chip style={{ marginLeft: 5, marginTop: 5 }} >Split by amount</Chip> */}
            </View>
            <View style={{ marginTop: 25, justifyContent: "center", flexDirection: "column", alignItems: "center" }}>

                {splitMembers.map((splitMember) => {
                    return (
                        <View style={{ width: "85%", marginTop: 10 }}>

                            <View key={splitMember.member} style={{ flexDirection: "row" }}>
                                <View style={{ flex: 1 }}>
                                    <Checkbox status={splitMember.status} onPress={handleSplitChangeCheckBox(splitMember.member)}></Checkbox>
                                </View>
                                <Text style={{ fontSize: 20, fontWeight: "400", flex: 1 }}>{splitMember.member}</Text>
                                <Text style={{ fontSize: 20, fontWeight: "400", flex: 1 }}>{expanse.amount}</Text>
                                <View style={{ flex: 1 }}>
                                    <TextInput
                                        style={{ backgroundColor: "white", height: 30, width: 50 }}
                                        keyboardType="numeric"
                                        onChangeText={handleSplitChangeInput(splitMember.member)}
                                        value={splitMember.devideWay}
                                    />
                                </View>
                            </View>
                            <Divider style={{ marginTop: 3 }}></Divider>

                        </View>

                    )
                })}
            </View>

  

        </View>
    )
}

export default SplitExpense;