import React, { useState, useEffect } from "react";
import { SafeAreaView, StyleSheet, Text, View, TextInput } from "react-native";
import { Button } from "react-native-paper";
import { firebase } from "./../../firebase/config";

interface Props {
    navigation: any
}

const Register: React.FC<Props> = (props) => {

    const [fullName, onChangFullName] = useState("");
    const [email, onChangeEmail] = useState("");
    const [password, onChangePassword] = useState("");
    const [confirmPassword, onChangeConfirmPassword] = useState("");
    const stackNavigator = (route: string) => {
        props.navigation.navigate(route);
    };

    //   const handleChange = (prop: keyof Post) => (event: React.ChangeEvent<HTMLInputElement>) => {
    //     setCurrentPost({ ...currentPost, [prop]: event.target.value });
    // };

    const onRegsiterPress = () => {
        if (password !== confirmPassword) {
            console.log("Passwords don't match.");
            return;
        }
        firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then((response) => {
                const uid = response.user?.uid;
                const data = {
                    id: uid,
                    email,
                    fullName,
                };
                const usersRef = firebase.firestore().collection("users");
                usersRef
                    .doc(uid)
                    .set(data)
                    .then(() => {
                        props.navigation.navigate("Home", { user: data });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return (
        // <SafeAreaView>
        <View style={styles.container}>
            <Text style={styles.appTitle}>Sign Up</Text>
            <TextInput
                style={styles.inputText}
                onChangeText={onChangFullName}
                value={fullName}
                placeholder="Full Name.."
            />
            <TextInput
                style={styles.inputText}
                onChangeText={onChangeEmail}
                value={email}
                placeholder="Email.."
            />
            <TextInput
                style={styles.inputText}
                onChangeText={onChangePassword}
                value={password}
                placeholder="Password.."
            />
            <TextInput
                style={styles.inputText}
                onChangeText={onChangeConfirmPassword}
                value={confirmPassword}
                placeholder="Password.."
            />
            {/* <Button style={styles.button} title="Login"></Button> */}
            <Button mode="contained" style={styles.button} onPress={onRegsiterPress}>
                Create account
            </Button>
            <View style={styles.signInText}>
                <Text>Already Have a user ?</Text>
                <Text style={styles.title} onPress={() => stackNavigator("Login")}>
                    Sign in
                </Text>
            </View>
        </View>
        /* </SafeAreaView> */
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: "35%"
        // backgroundColor: "#E8EAED",
        // marginBottom: "30px",
    },
    signInText: {
        marginTop: 10,
        flexDirection: "row",
    },
    appTitle: {
        fontWeight: "bold",
        fontSize: 40,
        paddingBottom: 15,
    },
    inputText: {
        height: 40,
        width: "85%",
        margin: 12,
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
    },
    password: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
    button: {
        marginTop: 30,
        backgroundColor: "#F55252",
        width: "85%",
        borderRadius: 10,
    },
    title: {
        color: "#acacac",
        fontWeight: "bold",
        textDecorationLine: "underline",
        marginLeft: 5,
    },
});

export default Register;
