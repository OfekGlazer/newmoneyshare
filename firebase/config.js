import firebase from "firebase";
import "@firebase/auth";
import "@firebase/firestore";

var firebaseConfig = {
  apiKey: "AIzaSyC6Alfir8FPbHjB3Gz9kOjClJA-aiWsF5I",
  authDomain: "moneyshare-46081.firebaseapp.com",
  projectId: "moneyshare-46081",
  storageBucket: "moneyshare-46081.appspot.com",
  databaseURL:
    "https://moneyshare-46081-default-rtdb.europe-west1.firebasedatabase.app/",
  messagingSenderId: "939383166788",
  appId: "1:939383166788:web:5098c3d14dcce63573a911",
  measurementId: "G-GKZFD8R5CT",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

export { firebase };

// <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-app.js"></script>

// <!-- TODO: Add SDKs for Firebase products that you want to use
//      https://firebase.google.com/docs/web/setup#available-libraries -->
// <script src="https://www.gstatic.com/firebasejs/8.9.1/firebase-analytics.js"></script>

// <script>
//   // Your web app's Firebase configuration
//   // For Firebase JS SDK v7.20.0 and later, measurementId is optional
//   var firebaseConfig = {
//     apiKey: "AIzaSyC6Alfir8FPbHjB3Gz9kOjClJA-aiWsF5I",
//     authDomain: "moneyshare-46081.firebaseapp.com",
//     projectId: "moneyshare-46081",
//     storageBucket: "moneyshare-46081.appspot.com",
//     messagingSenderId: "939383166788",
//     appId: "1:939383166788:web:5098c3d14dcce63573a911",
//     measurementId: "G-GKZFD8R5CT"
//   };
//   // Initialize Firebase
//   firebase.initializeApp(firebaseConfig);
//   firebase.analytics();
// </script>
