
import React, { useState, useEffect, createContext } from 'react';
import { Text, TouchableOpacity, StyleSheet, View, SafeAreaView } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Group, User } from './Models/General';
import { checkUserSession } from './screens/Service';
import MainNavigation from "./Routing/MainNavigation";
import { userContext } from "./AppContext";

const Stack = createNativeStackNavigator();

// export const userContext = createContext<User | undefined | null>(undefined)

const App = () => {

  const [user, setUser] = useState<User | undefined | null>(undefined);
  const [groupInWork, setGroupInWork] = useState<Group | undefined>(undefined);
  // const [user, setUser] = useState<User | undefined | null>(undefined);

  useEffect(() => {
    checkUserSession(setUser)
  }, [])

  return (
    <userContext.Provider value={{ user, setUser, groupInWork: groupInWork, setGroupInWork: setGroupInWork }}>
      <SafeAreaView style={styles.container}>
        <MainNavigation />
      </SafeAreaView>
    </userContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    // alignItems: '',
    justifyContent: 'center',
  },
  startGroupButton: {
    fontSize: 18,
    marginRight: 10,
    color: "#2970E9"
  }
})

export default App;



// return (
//   <NavigationContainer>
//     <userContext.Provider value={user}>

//       <Stack.Navigator>
//         <Stack.Screen name="Home" component={Home} />
//         <Stack.Screen name="Login" component={Login} options={{ title: 'Login' }} />
//         <Stack.Screen name="Regsiter" component={Regsiter} options={{ title: 'Regsiter' }} />
//         <Stack.Screen name="AddGroup" component={AddGroup} />
//         <Stack.Screen name="AllGroups" component={AllGroups} />
//         <Stack.Screen name="AddMember" component={AddMember} options={
//           ({ navigation }) => ({
//             title: 'Add Member',

//             headerRight: () => (

//               <TouchableOpacity
//                 onPress={() => navigation.navigate('AddGroup')}>
//                 <Text style={styles.startGroupButton}>Done</Text>
//               </TouchableOpacity>
//             ),
//           })
//         } />
//       </Stack.Navigator>
//     </userContext.Provider>
//   </NavigationContainer>
// );